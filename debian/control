Source: libgfshare
Section: devel
Priority: optional
Maintainer: Simon McVittie <smcv@debian.org>
Standards-Version: 4.5.1
Build-Depends:
 debhelper-compat (= 12),
 dpkg-dev (>= 1.16.1),
 dvipng <!nodoc>,
 faketime <!nodoc>,
 tex4ht <!nodoc>,
 texlive-latex-base <!nodoc>,
 texlive-latex-recommended <!nodoc>,
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/libgfshare.git
Vcs-Browser: https://salsa.debian.org/debian/libgfshare
Homepage: https://git.gitano.org.uk/libgfshare.git/

Package: libgfshare-bin
Architecture: any
Section: utils
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: utilities for multi-way secret-sharing
 This package contains the gfsplit and gfcombine binaries.
 .
 gfsplit can be used to split a secret file (like a GPG secret key)
 into multiple parts, so that at least some number of those parts are needed
 to recover the original file, but any smaller combination of parts is useless
 to an attacker. gfcombine is used to combine the parts created by gfsplit.
 .
 For instance, you might split a GPG key using a 3-of-5 share and put one
 share on each of three computers, and two shares on a USB memory stick; then
 you can use the GPG key on any of those three computers using the memory
 stick, but if the memory stick is lost, you can recover the key by
 bringing the three computers together.

Package: libgfshare-dev
Architecture: any
Section: libdevel
Depends:
 libgfshare2 (= ${binary:Version}),
 ${misc:Depends},
Suggests:
 libgfshare-bin,
Replaces:
 libgfshare1 (<< 1.0.2-1),
Description: library for multi-way secret-sharing (headers)
 This library implements Shamir's method for secret sharing, which can be
 used to split a secret (like a GPG secret key) into multiple parts, so that
 at least some number of those parts are needed to recover the original file,
 but any smaller combination of parts is useless to an attacker.
 .
 This package contains header files and static libraries, needed to compile
 programs that use libgfshare.

Package: libgfshare2
Multi-Arch: same
Architecture: any
Section: libs
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 libgfshare-bin,
Description: library for multi-way secret-sharing
 This library implements Shamir's method for secret sharing, which can be
 used to split a secret (like a GPG secret key) into multiple parts, so that
 at least some number of those parts are needed to recover the original file,
 but any smaller combination of parts is useless to an attacker.
 .
 For instance, you might split a GPG key using a 3-of-5 share and put one
 share on each of three computers, and two shares on a USB memory stick; then
 you can use the GPG key on any of those three computers using the memory
 stick, but if the memory stick is lost, you can recover the key by
 bringing the three computers together.
